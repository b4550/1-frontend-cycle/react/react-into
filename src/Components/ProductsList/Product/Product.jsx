import React from "react";

const Product = ({ product }) => {
  return (
    <div>
      <img
        className="product__image"
        src={product?.image}
        alt={product?.title}
      />
      <div className="flex">
        <h3>{product.title}</h3>
        <p>{product.price}</p>
      </div>
      <p>Availability: {product.count}</p>
      <button style={{backgroundColor: `${product.count ? '#4D96FF' : '#ccc'}`, color: 'white', padding: '10px'}}>{product.count ? "ADD TO CART" : "NOT AVAILABLY"}</button>
    </div>
  );
};

export default Product;
